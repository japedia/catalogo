from django import forms

class CrearProductoForm(forms.Form):
    nombre = forms.CharField()
    imagen = forms.ImageField()
